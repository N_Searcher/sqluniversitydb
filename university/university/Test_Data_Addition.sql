use University
go
create or alter procedure AddStudent
@f_name nvarchar(30), @l_name nvarchar(30), @group_name nvarchar(30) 
as
begin
	declare @g_id int
	select @g_id = Groups.Group_Id from Groups where Groups.Name = @group_name
	insert into Students(Group_Id, First_Name, Last_Name)
	values(@g_id, @f_name, @l_name)
end

go

create or alter procedure AddGroups
@course_name nvarchar(30), @name nvarchar(30)
as
begin
	insert into Groups(Name, Course_Id)
	values(@name, (select top 1 Course.Course_Id from Course where Course.Name = @course_name))
end

go

create or alter procedure AddCourse
@course_name nvarchar(30)
as
begin
	declare @description nvarchar(max)
	set @description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
nisi ut aliquip ex ea commodo consequat. 
Duis aute irure dolor in reprehenderit in voluptate velit esse 
cillum dolore eu fugiat nulla pariatur. 
Excepteur sint occaecat cupidatat non proident, 
sunt in culpa qui officia deserunt mollit anim id est laborum.'
	insert into Course(Name, Description)
	values (@course_name, @description)
end

go

create or alter procedure AddCourses
as
begin
	execute AddCourse '.net'
	execute AddCourse 'java'
	execute AddCourse 'python'
	execute AddCourse 'JavaScript'
	execute AddCourse 'c++'
	execute AddCourse 'php'
end

go
execute AddCourses
go
execute AddGroups '.net', 'SR-02'
go
execute AddGroups 'c++', 'SR-450'
go
execute AddGroups 'php', 'SR-15'
go
execute AddGroups 'php', 'SR-03'
go
execute AddGroups 'JavaScript', 'SR-11'
go
execute AddStudent 'Bogdan', 'Kovalyuk', 'SR-02'
go
execute AddStudent 'Gor', 'Egyptian', 'SR-450'
go
execute AddStudent 'Seth', 'Egyptian', 'SR-450'
go
execute AddStudent 'Sebek', 'Egyptian', 'SR-450'
go
execute AddStudent 'Inpu', 'Egyptian', 'SR-15'
go
execute AddStudent 'Ra', 'Egyptian', 'SR-15'
go
execute AddStudent 'Osiris', 'Egyptian', 'SR-02'
go
execute AddStudent 'Ptah', 'Egyptian', 'SR-02'
go
execute AddStudent 'Hathor', 'Egyptian', 'SR-02'
go
execute AddStudent 'Thoth', 'Egyptian', 'SR-03'
go
execute AddStudent 'Amon', 'Egyptian', 'SR-11'
