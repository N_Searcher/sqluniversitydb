if not exists (select * from sys.databases where name = 'University')
begin
	create database University
end

go
use University

go
create or alter procedure CourseCreation as 
begin
	drop table if exists Course

	create table Course
	(
		Course_Id int identity(1, 1) primary key,
		Name nvarchar(30) not null,
		Description nvarchar(max) not null
	)
end

go
create or alter  procedure GroupsCreation as 
begin
	drop table if exists Groups

	create table Groups
	(
		Group_Id int identity(1, 1) primary key,
		Name nvarchar(30) not null,
		Course_Id int not null,
		constraint FK_Groups_Courses foreign key(Course_Id) references Course(Course_Id)
	)
end

go
create or alter  procedure StudentsCreation as
begin
	drop table if exists Students

	create table Students
	(
		Student_Id int identity(1,1) primary key,
		Group_Id int,
		First_Name nvarchar(30) not null,
		Last_Name nvarchar(30) not null,
		constraint FK_Students_Group foreign key(Group_Id) references Groups(Group_Id)
	)
end

go
execute CourseCreation

go
execute GroupsCreation

go
execute StudentsCreation
