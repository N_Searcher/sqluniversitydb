use University

go
create or alter procedure FindAlmostEmptyGroups
as
begin
	select Groups.Name as [Group], count(Students.Student_Id) as Students_Quantity
	from Groups
	join Students
	on Groups.Group_Id = Students.Group_Id
	group by Groups.Name
	having count(Students.Student_Id) < 10
end

go
execute FindAlmostEmptyGroups

go
create or alter procedure DeleteStudentsFromGroup
@group_name nvarchar(30)
as
begin
	delete from Students
	where 
	(select top 1 Groups.Group_Id from Groups where Groups.Name = @group_name) = Students.Group_Id
end

go
execute DeleteStudentsFromGroup 'SR-02'

go
create or alter procedure GetCourseAndStudents
as
begin
	select Course.Name as [Course], Students.First_Name as FirstName, Students.Last_Name as LastName
	from Students
	join Groups
	on Students.Group_Id = Groups.Group_Id
	join Course
	on Course.Course_Id = Groups.Course_Id
	order by [Course]
end

go
execute GetCourseAndStudents

